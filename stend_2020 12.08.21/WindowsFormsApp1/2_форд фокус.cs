﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class форд_фокус : Form
    {

        string ComPortSelect;
        static Boolean openPort = false;
        static SerialPort PortSelect;
        bool GoInData = false;
        bool GoOshibki = false;
        bool GoPitanie = false;
        bool Go15Kl = false;
        bool GoZapuck = false;
        bool gobar = false;
        public string dataCom;
        public int time_form_ms = 0;
        public int i = 0;
        public int b3 = 0;
        public string напр = "0";
        public string ток = "0";
        public string[] CanData;
        public string[] MomentNaValu = { "130", "22", "11", "11" };        // 082 80 00 14 24 80 00 00 00 80(128)-грубо середина датчик fd(253) мах 00(00) мин 
        public string[] UgolPovorotaRotora = { "132", "22", "11", "11", "11" };  //

        public string[] OshibkiID = { "1848", "6", "89", "1", "202" }; //  738 05 59 01 ca 00 00 zz 00      5000 0 8 1848 5 89 1 202 0 0 17 0  
                                                                       //1848 6 89 1 202 0 0 2 0
        public string[] OshibkiID1 = { "1848", "16", "17", "89", "2", "202", "193" }; //  738 10    59 02 ca c1 00 00      5000 0 8 1848 16 17 89 2 202 193 0 0

        uint tik = 0; // пакет 
        int step_can_pusk = 0;
        int max_step = 10;
        public string[] CanPusk = { };
        public string[] Can15on = { };
        public string[] Can_send1;

        string[,] mas_data_oshibki = new string[9, 6];


        public форд_фокус()
        {
            InitializeComponent();
            string[] ports = SerialPort.GetPortNames();
            ComSelect.Items.AddRange(ports);
            button1.Text = ("Выберите порт");
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 150;
            progressBar4.Minimum = 0;
            progressBar4.Maximum = 5000;
            progressBar2.Minimum = 0;
            progressBar2.Maximum = 5000;
            progressBar3.Minimum = 0;
            progressBar3.Maximum = 150;

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComPortSelect = ComSelect.SelectedItem.ToString();
            button1.Text = ("Открыть порт");
        }

        private void button2_Click(object sender, EventArgs e)
        {
           

        }

        async void oshibki_opros()
        {
            int j = 0;
            int step = 1;
            GoInData = false;

            for (int ip = 0; ip < 6; ip++)
            {
                for (int jp = 0; jp < 9; jp++)
                {
                    mas_data_oshibki[jp, ip] = "0";
                }
            }


            await Task.Delay(1000);
            await Task.Run(() => {
                while (GoOshibki) //&& time_form_ms==4900  
                {

                    Invoke(new Action(() => dataCom = PortSelect.ReadLine()));
                    //Invoke(new Action(() => textBox1.Text += dataCom + Environment.NewLine));
                    CanData = dataCom.Split(' ');
                    //dataCom = dataCom.TrimEnd('\r');
                    //(CanData[0]== MomentNaValu [0]

                    if (step == 1)
                    {

                        Invoke(new Action(() => textBox1.Text += "Запрос отправлен " + Environment.NewLine));
                        Invoke(new Action(() => PortSelect.Write("$5000 0 8 1840 3 25 1 143 0 0 0 0;")));
                        step = 2;
                        // ТУТ НЕ ДОПИСАНО
                    }

                    if (CanData[0] == OshibkiID[0] && CanData[1] == OshibkiID[1] && CanData[2] == OshibkiID[2] && step == 2)  // 1848 6 89 1 202 0 0 2 0
                    {
                        Invoke(new Action(() => textBox1.Text += "ошибок в блоке = " + CanData[7] + Environment.NewLine));
                        Invoke(new Action(() => PortSelect.Write("$5000 0 8 1840 3 25 2 143 0 0 0 0;")));
                        step = 3;

                    }
                    if (CanData[0] == OshibkiID1[0] && CanData[1] == OshibkiID1[1] && CanData[3] == OshibkiID1[3] && step == 3)
                    {
                        int x = Convert.ToInt32(CanData[6], 10); string s = x.ToString("x");
                        int dl = s.Length; if (dl == 1) s = "0" + s;
                        mas_data_oshibki[4, 5] = s;
                        //Invoke(new Action(() => textBox1.Text += s + " "));
                        x = Convert.ToInt32(CanData[7], 10); s = x.ToString("x");
                        dl = s.Length; if (dl == 1) s = "0" + s; ;
                        mas_data_oshibki[5, 5] = s;
                        //Invoke(new Action(() => textBox1.Text += s + " "));
                        CanData[8] = CanData[8].TrimEnd('\r'); x = Convert.ToInt32(CanData[8], 10); s = x.ToString("x");
                        dl = s.Length; if (dl == 1) s = "0" + s; ;
                        mas_data_oshibki[6, 5] = s;
                        // Invoke(new Action(() => textBox1.Text += s + " "));
                        //  Invoke(new Action(() => textBox1.Text += Environment.NewLine));
                        Invoke(new Action(() => PortSelect.Write("$5000 0 8 1840 48 0 0 0 0 0 0 0;")));
                        step = 4;
                    }

                    if (step == 4 && CanData[0] == OshibkiID1[0] && (CanData[1] == "33" || CanData[1] == "34" || CanData[1] == "35" || CanData[1] == "36" || CanData[1] == "37"))
                    {
                        //    10 13 59 02 ca c1 00 
                        //738 10 1f 59 02 ca c1 0 0 

                        if (CanData[1] == "16" && CanData[3] == "89" && CanData[4] == "2" && CanData[5] == "202" && CanData[6] == "193")
                        {
                            // захватывает предыдущую строку из за дублирования по ней и смотрим парковку

                            if (CanData[2] == "19")
                            {
                                // Invoke(new Action(() => textBox1.Text += "Функция автоматической парковки отключена" + Environment.NewLine));
                                //без парковки
                            }
                            else if (CanData[2] == "27")
                            {
                                // Invoke(new Action(() => textBox1.Text += "Функция автоматической парковки активна" + Environment.NewLine));
                                //c парковкой
                            }
                            else
                            {

                            }

                        }
                        else
                        {

                            int length = CanData.Length;
                            for (int i = 0; i < length; i++)
                            {
                                CanData[i] = CanData[i].TrimEnd('\r');
                                int x = Convert.ToInt32(CanData[i], 10);
                                string s = x.ToString("x");
                                int dl = s.Length;
                                if (dl == 1)
                                {
                                    s = "0" + s;
                                }
                                //Invoke(new Action(() => textBox1.Text += s + " "));
                                mas_data_oshibki[i, j] = s;


                            }
                            j++;
                            // Invoke(new Action(() => textBox1.Text += Environment.NewLine));

                        }

                    }

                }

            });

        }

        public void dataIn()
        {
            while (GoInData)  
            {
                if (PortSelect.BytesToRead != 0)
                {
                    dataCom = PortSelect.ReadLine();
                    CanData = dataCom.Split(' ');

                    if (CanData[0] == UgolPovorotaRotora[0]) { UgolPovorotaRotora[3] = CanData[7]; UgolPovorotaRotora[4] = CanData[8].TrimEnd('\r'); }

                    else if (CanData[0] == MomentNaValu[0]){ MomentNaValu[1] = CanData[1]; }  // 1 байт

                    else if (CanData[0] == "1848" && CanData[1] == "16" && CanData[2] == "27" && CanData[3] == "98" && CanData[4] == "241")
                        {
                         if (CanData[6] == "0" || CanData[7] == "0") { Invoke(new Action(() => textBox1.Text += "Зафиксированно повреждение ПЗУ" + Environment.NewLine));}
                         else {Invoke(new Action(() => textBox1.Text += "F113 версия блока управления: " + Convert.ToChar(Convert.ToInt16(CanData[6])) + "" + Convert.ToChar(Convert.ToInt16(CanData[7])) + Environment.NewLine));}
                        }

                    else if (CanData[0] == "8888")  { напр = CanData[1]; ток = CanData[2];}

                }

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Go15Kl && GoOshibki == false)
            {
                for (int j = 0; j < 4; j++)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        mas_data_oshibki[i, j] = "0";

                    }

                }
                Invoke(new Action(() => textBox1.Clear()));
                time_form_ms = 0;
                button7.Text = "Удалить коды неисправностей";
                // var oshibkiGo = new Thread(oshibki_opros);
                GoOshibki = true;
                oshibki_opros();
                //oshibkiGo.Start();
                if (true)
                {

                }
            }
            else if (Go15Kl == false) { button3.Text = "Включите зажигание!"; }
            else if (GoOshibki) { button3.Text = "Ожидайте 2 секунд"; }
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            b3++;

            if (b3 == 500)
            {
                gobar = true;
            }

            if (b3 == 2000)
            {
                b3 = 0;
                chart1.Series["Нм"].Points.Clear();
                chart1.Series["Нм"].Points.AddXY(2000, 0);
                chart2.Series["угол поворота"].Points.Clear();
                chart2.Series["угол поворота"].Points.AddXY(2000, 0);
            }

            if (GoInData && Go15Kl && gobar)
            {

                if (Convert.ToInt32(MomentNaValu[1], 10) <= 126)  //128
                {
                    progressBar1.Value = 0;
                    progressBar3.Value = Math.Abs(126 - (Convert.ToInt32(MomentNaValu[1], 10)));
                }
                else if (Convert.ToInt32(MomentNaValu[1], 10) >= 130 && Convert.ToInt32(MomentNaValu[1], 10) <= 254)
                {
                    progressBar3.Value = 0;
                    progressBar1.Value = Math.Abs((Convert.ToInt32(MomentNaValu[1], 10)) - 130);
                }
                UgolPovorotaRotora[4] = UgolPovorotaRotora[4].TrimEnd('\r');
                int Ugol_obrabotan = (Convert.ToInt32(((Convert.ToInt32(UgolPovorotaRotora[3], 10) * 255) + Convert.ToInt32(UgolPovorotaRotora[4], 10) - 31875) * 2.23603));


                if (Ugol_obrabotan >= 1 && Ugol_obrabotan < 4900)
                {
                    progressBar2.Value = Ugol_obrabotan;
                    progressBar4.Value = 0;
                    Invoke(new Action(() => chart2.Series["угол поворота"].Points.AddXY(b3, Ugol_obrabotan)));
                    textBox3.Text = Convert.ToString(Ugol_obrabotan) + " градусов";
                }
                else if (Ugol_obrabotan <= -1 && Ugol_obrabotan > -4900)
                {
                    progressBar4.Value = Math.Abs(Ugol_obrabotan);
                    progressBar2.Value = 0;
                    Invoke(new Action(() => chart2.Series["угол поворота"].Points.AddXY(b3, Ugol_obrabotan)));
                    textBox3.Text = Convert.ToString(Ugol_obrabotan) + " градусов";
                }


                else
                {
                    progressBar2.Value = 0;
                    progressBar4.Value = 0;
                    textBox3.Text = "Недоступен";
                }
                double moment = Math.Round(((Convert.ToDouble(MomentNaValu[1]) - 128) / 12), 2);
                textBox2.Text = moment + " Нм";
                // chart1.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
                Invoke(new Action(() => chart1.Series["Нм"].Points.AddXY(b3, moment)));

            }

            else
            {
                chart1.ChartAreas[0].AxisY.Maximum = 3;
                chart1.ChartAreas[0].AxisY.Minimum = -3;
                chart1.ChartAreas[0].AxisX.Maximum = 2000;
                chart1.ChartAreas[0].AxisX.Minimum = 0;
                progressBar1.Value = 0;
                progressBar3.Value = 0;
                progressBar2.Value = 0;
                textBox2.Text = "Датчик момента";
                textBox3.Text = "Угол поворота";
                chart2.ChartAreas[0].AxisY.Maximum = 5000;
                chart2.ChartAreas[0].AxisY.Minimum = -5000;
                chart2.ChartAreas[0].AxisX.Maximum = 2000;
                chart2.ChartAreas[0].AxisX.Minimum = 0;

            }

            if (true)  // возможно к чему то привюжу
            {
                // 8888 чч уу    чч - напряжение уу - ток

                if (Convert.ToInt32(напр) <= 3300 && Convert.ToInt32(ток) <= 3300)
                {
                    наряжение_бар.Value = Convert.ToInt32(напр);
                    напряжение_текст.Text = Convert.ToString(напр);
                    ток_бар.Value = Convert.ToInt32(ток);
                    ток_текст.Text = Convert.ToString(ток);
                }
                else
                {
                    напряжение_текст.Text = "диапазон";
                    ток_текст.Text = "превышен";
                }



                //для нарпяжения и тока


            }


        }

        private void button4_Click(object sender, EventArgs e)
        {
            Invoke(new Action(() => PortSelect.Write("$5000 0 8 1840 3 34 241 19 0 0 0 0;")));

            // запуск питания 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (GoPitanie)
            {
                Go15Kl = !Go15Kl;
                if (Go15Kl)
                {
                    PortSelect.Write("$7117; $6006;");
                    PortSelect.Write("$1500;");
                    button3.Text = "Чтение ошибок";
                    Зажигание_15.Text = "Зажигание активно";
                    Зажигание_15.BackColor = Color.FromArgb(0, 255, 0);

                }
                else
                {

                    PortSelect.Write("$4500;");
                    PortSelect.Write("$7007; $6006;");
                    GoZapuck = false;
                    GoOshibki = false;
                    Данные_отправка.Text = "Двигател заглушен";
                    Данные_отправка.BackColor = Color.Transparent;
                    Зажигание_15.Text = "Зажигание отключено";
                    Зажигание_15.BackColor = Color.Transparent;

                }
            }
            // зжигание
        }

        private void Данные_отправка_Click(object sender, EventArgs e)
        {
            if (GoPitanie && Go15Kl)
            {
                GoZapuck = !GoZapuck;
                if (GoZapuck)
                {
                    PortSelect.Write("$7007; $6116;");
                    Данные_отправка.Text = "Двигател заведён";
                    Данные_отправка.BackColor = Color.FromArgb(0, 255, 0);
                    /*/ myTimer.Enabled = true;
                     can_send.Enabled = true;
                     button6.Text = "Двигател заведён";
                     tik = 0;
                     max_step = CanPusk.Length;
                     Can_send1 = CanPusk[0].Split('-');
                     step_can_pusk = 0;
                    */
                }
                else
                {
                    can_send.Enabled = false;
                    Данные_отправка.Text = "Двигател заглушен";
                    Данные_отправка.BackColor = Color.Transparent;
                    Invoke(new Action(() => PortSelect.Write("$6006; $7117;")));

                }


            }
            // запуск
        }

        private void button7_Click(object sender, EventArgs e)
        {
            // 730 04 14 ff ff ff 00 00 00
            // 1840 4 20 255 255 255 0 0 0
            Invoke(new Action(() => PortSelect.Write("$5000 0 8 1840 4 20 255 255 255 0 0 0;")));
            button7.Text = "Ошибки удалены!";
            // удалить ошибки 

        }

        private void can_send_Tick(object sender, EventArgs e)
        {
            // 10-5000 0 8 115 40 32 58 88 0 128 0 247
            //Can_send1[0] = 10;

            // PortSelect.Write("$5000 0 8 65 0 0 0 0 0 0 0;");
            // 15-20 период

           


            // 5000 0 8 936 0 0 0 0 0 0 0
            //Invoke(new Action(() => PortSelect.Write("5000 0 8 936 0 0 0 0 0 0 0")));
            //Can_send1 = CanPusk[step_can_pusk].Split('-');
            // tik++;
            // step_can_pusk++;
            //Invoke(new Action(() => PortSelect.Write(Can_send1[1])));
            //if (tik==499)
            // {
            //    tik = 0;
            //    step_can_pusk = 0;
            // }

        }

        private void ComSelect_SelectedIndexChanged(object sender, EventArgs e)
        {

            ComPortSelect = ComSelect.SelectedItem.ToString();
            button1.Text = ("Открыть порт");

        }

        private void button1_Click(object sender, EventArgs e)
        {

            var dataInGou = new Thread(dataIn);

            if (openPort)
            {
                button2.Text = ("Поток закрыт");
                button2.BackColor = Color.Transparent;
                GoInData = false;
                dataInGou.Abort();
                openPort = false;
                Thread.Sleep(500);
                PortSelect.DiscardInBuffer();
                PortSelect.DiscardOutBuffer();
                PortSelect.Close();
                button1.Text = ("Порт закрыт");
                button1.BackColor = Color.Transparent;
            }
            else
            {
                /*
                 *  // Все опции для последовательного устройства
                    // ---- могут быть отправлены через конструктор класса SerialPort
                    // ---- PortName = "COM1", Baud Rate = 19200, Parity = None,
                    // ---- Data Bits = 8, Stop Bits = One, Handshake = None
                        SerialPort _serialPort = new SerialPort("COM1", 
                                        19200, 
                                        Parity.None,
                                        8,
                                        StopBits.One);
                    _serialPort.Handshake = Handshake.None;
                 * 
                 */

                PortSelect = new SerialPort(ComPortSelect, 250000);
                PortSelect.Open();
                PortSelect.DiscardInBuffer();
                PortSelect.DiscardOutBuffer();
                PortSelect.ReadTimeout = 300;
                PortSelect.WriteTimeout = 300;
                button1.Text = ("Порт открыт");
                button1.BackColor = Color.FromArgb(0, 255, 0);
                openPort = true;
                button2.Text = ("");
                button2.Text = ("Выгрузить данные");
                Thread.Sleep(300);
                GoInData = true;
                dataInGou.Start();
            }
        }

        private void Pitanie_Click(object sender, EventArgs e)
        {
            GoPitanie = !GoPitanie;
            if (GoPitanie)
            {
                PortSelect.Write("$1000 10;");
                Питание_30.BackColor = Color.FromArgb(0, 255, 0);
                Питание_30.Text = "Питание активно";
            }
            else
            {
                PortSelect.Write("$8000;");
                PortSelect.Write("$3000;");
                Данные_отправка.Text = "Двигател заглушен";
                Данные_отправка.BackColor = Color.Transparent;
                Зажигание_15.Text = "Зажигание отключено";
                Зажигание_15.BackColor = Color.Transparent;
                Питание_30.Text = "Питание отключено";
                Питание_30.BackColor = Color.Transparent;
                GoZapuck = false;
                Go15Kl = false;

            }
        }

        private void форд_фокус_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0000.jpg");
            pictureBox1.Show(); 
            string путь_к_файлу = @"C: \Users\Public\Documents\file_gs.txt";
            StreamReader файл_в_корне = new StreamReader(путь_к_файлу, Encoding.Default);
            ComPortSelect = файл_в_корне.ReadLine();
            файл_в_корне.Close();
        }

        private void форд_фокус_Closed(object sender, EventArgs e)
        {

        }

        public string Dorabotka_oshibok(char dov1, char dov2, int i, int j)
        {

            switch (dov1)
            {
                case '0':
                    mas_data_oshibki[i, j] = "P0" + dov2;
                    break;
                case '1':
                    mas_data_oshibki[i, j] = "P1" + dov2;
                    break;
                case '2':
                    mas_data_oshibki[i, j] = "P2" + dov2;
                    break;
                case '3':
                    mas_data_oshibki[i, j] = "P3" + dov2;
                    break;
                case '4':
                    mas_data_oshibki[i, j] = "C0" + dov2;
                    break;
                case '5':
                    mas_data_oshibki[i, j] = "C1" + dov2;
                    break;
                case '6':
                    mas_data_oshibki[i, j] = "C2" + dov2;
                    break;
                case '7':
                    mas_data_oshibki[i, j] = "C3" + dov2;
                    break;
                case '8':
                    mas_data_oshibki[i, j] = "B0" + dov2;
                    break;
                case '9':
                    mas_data_oshibki[i, j] = "B1" + dov2;
                    break;
                case 'a':
                    mas_data_oshibki[i, j] = "B2" + dov2;
                    break;
                case 'b':
                    mas_data_oshibki[i, j] = "B3" + dov2;
                    break;
                case 'c':
                    mas_data_oshibki[i, j] = "U0" + dov2;
                    break;
                case 'd':
                    mas_data_oshibki[i, j] = "U1" + dov2;
                    break;
                case 'e':
                    mas_data_oshibki[i, j] = "U2" + dov2;
                    break;
                case 'f':
                    mas_data_oshibki[i, j] = "U3" + dov2;
                    break;
                default:
                    break;
            }
            return mas_data_oshibki[i, j];
        }

        private void time_form_Tick(object sender, EventArgs e)
        {

            time_form_ms++;

            if (openPort)
            {
                byte_to_write_text.Text = Convert.ToString(PortSelect.BytesToRead);

                byte_to_read_text.Text = Convert.ToString(PortSelect.BytesToWrite);
                if (PortSelect.BytesToRead >= 5000)
                {
                    PortSelect.DiscardInBuffer();
                }
                else byte_to_write.Value = (PortSelect.BytesToRead);

                if (PortSelect.BytesToWrite >= 5000)
                {
                    PortSelect.DiscardOutBuffer();
                }
                else byte_to_read.Value = (PortSelect.BytesToWrite);
            }

            //button4.Text = time_form_ms.ToString();
            if (time_form_ms == 100)
            {
                if (button3.Text == "Ожидайте 2 секунд") button3.Text = "Чтение ошибок";
                time_form_ms = 0;

                if (GoOshibki)
                {
                    GoOshibki = false;
                    GoInData = true;
                    var dataInGou = new Thread(dataIn);
                    dataInGou.Start();
                    char dov1 = '0';
                    char dov2 = '0';

                    StringBuilder Dovodka_oshibki_srt;
                    if (mas_data_oshibki[3, 0] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[3, 0]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; mas_data_oshibki[3, 0] = Dorabotka_oshibok(dov1, dov2, 3, 0);
                    }
                    if (mas_data_oshibki[7, 0] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[7, 0]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; mas_data_oshibki[7, 0] = Dorabotka_oshibok(dov1, dov2, 7, 0);
                    }
                    if (mas_data_oshibki[4, 1] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[4, 1]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; mas_data_oshibki[4, 1] = Dorabotka_oshibok(dov1, dov2, 4, 1);
                    }
                    if (mas_data_oshibki[8, 1] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[8, 1]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; mas_data_oshibki[8, 1] = Dorabotka_oshibok(dov1, dov2, 8, 1);
                    }
                    if (mas_data_oshibki[5, 2] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[5, 2]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; mas_data_oshibki[5, 2] = Dorabotka_oshibok(dov1, dov2, 5, 2);
                    }
                    if (mas_data_oshibki[2, 3] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[2, 3]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; if (dov1 != '0') mas_data_oshibki[2, 3] = Dorabotka_oshibok(dov1, dov2, 2, 3);
                    }
                    if (mas_data_oshibki[6, 3] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[6, 3]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; if (dov1 != '0') mas_data_oshibki[6, 3] = Dorabotka_oshibok(dov1, dov2, 6, 3);
                    }
                    if (mas_data_oshibki[3, 4] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[3, 4]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; if (dov1 != '0') mas_data_oshibki[3, 4] = Dorabotka_oshibok(dov1, dov2, 3, 4);
                    }
                    if (mas_data_oshibki[7, 4] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[7, 4]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; if (dov1 != '0') mas_data_oshibki[7, 4] = Dorabotka_oshibok(dov1, dov2, 7, 4);
                    }
                    if (mas_data_oshibki[4, 5] != "0")
                    {
                        Dovodka_oshibki_srt = new StringBuilder(mas_data_oshibki[4, 5]); dov1 = Dovodka_oshibki_srt[0];
                        dov2 = Dovodka_oshibki_srt[1]; if (dov1 != '0') mas_data_oshibki[4, 5] = Dorabotka_oshibok(dov1, dov2, 4, 5);
                    }

                    string[] oshibki = { mas_data_oshibki[3, 0] + mas_data_oshibki[4, 0] + mas_data_oshibki[5, 0]+ mas_data_oshibki[6, 0], //0+
                                         mas_data_oshibki[7, 0] + mas_data_oshibki[8, 0] + mas_data_oshibki[2, 1]+ mas_data_oshibki[3, 1], //1+
                                         mas_data_oshibki[4, 1] + mas_data_oshibki[5, 1] + mas_data_oshibki[6, 1]+ mas_data_oshibki[7, 1], //2+
                                         mas_data_oshibki[8, 1] + mas_data_oshibki[2, 2] + mas_data_oshibki[3, 2]+ mas_data_oshibki[4, 2], //3+
                                         mas_data_oshibki[5, 2] + mas_data_oshibki[6, 2] + mas_data_oshibki[7, 2]+ mas_data_oshibki[8, 2], //4+
                                         mas_data_oshibki[2, 3] + mas_data_oshibki[3, 3] + mas_data_oshibki[4, 3]+ mas_data_oshibki[5, 3], //5+
                                         mas_data_oshibki[6, 3] + mas_data_oshibki[7, 3] + mas_data_oshibki[8, 3]+ mas_data_oshibki[2, 4], //6+
                                         mas_data_oshibki[3, 4] + mas_data_oshibki[4, 4] + mas_data_oshibki[5, 4]+ mas_data_oshibki[6, 4],  //7+
                                         mas_data_oshibki[7, 4] + mas_data_oshibki[8, 4] + mas_data_oshibki[2, 5]+ mas_data_oshibki[3, 5],  //8+
                                         mas_data_oshibki[4, 5] + mas_data_oshibki[5, 5] + mas_data_oshibki[6, 5]+ mas_data_oshibki[2, 0]  //9 - первая короч
                };

                    Invoke(new Action(() => textBox1.Text += "Ошибки в блоке управления:" + Environment.NewLine));
                    for (int i = 0; i < 10; i++)
                    {

                        //Invoke(new Action(() => textBox1.Text += oshibki[i] + Environment.NewLine));
                        if (oshibki[i].Length > 4)
                        {
                            switch (oshibki[i])
                            {
                                // Модуль ЭУР внутреняя электронная неисправность
                                case "U3000498a":
                                    Invoke(new Action(() => textBox1.Text += "U3000 - Модуль ЭУР внутреняя электронная неисправность" + Environment.NewLine));
                                    break;
                                case "U30004948":
                                    Invoke(new Action(() => textBox1.Text += "U3000 - Модуль ЭУР внутреняя электронная неисправность" + Environment.NewLine));
                                    break;
                                case "U300053c8":
                                    Invoke(new Action(() => textBox1.Text += "U3000 - Модуль ЭУР дезактивирован" + Environment.NewLine));
                                    break;
                                case "U300096c8":
                                    Invoke(new Action(() => textBox1.Text += "U3000 - Модуль ЭУР внутреняя электронная неисправность" + Environment.NewLine));
                                    break;
                                case "U20114948":
                                    Invoke(new Action(() => textBox1.Text += "U2011 - Внутреняя электронная неисправность электродвигателя" + Environment.NewLine));
                                    break;
                                // шана кан
                                case "U00018808":
                                    Invoke(new Action(() => textBox1.Text += "U0001 - Зафиксированно отключение шины HS-can" + Environment.NewLine));
                                    break;
                                case "U0001880a":
                                    Invoke(new Action(() => textBox1.Text += "U0001 - Зафиксированно отключение шины HS-can" + Environment.NewLine));
                                    break;
                                //Модуль вспомагательноей удер системы
                                case "U01510008":
                                    Invoke(new Action(() => textBox1.Text += "U0151 - Нет связи с вспомогательной удерживающей системой" + Environment.NewLine));
                                    break;
                                case "U0151000a":
                                    Invoke(new Action(() => textBox1.Text += "U0151 - Нет связи с вспомогательной удерживающей системой" + Environment.NewLine));
                                    break;
                                // АБС
                                case "U01210008":
                                    Invoke(new Action(() => textBox1.Text += "U0121 - Нет связи с абс " + Environment.NewLine));
                                    break;
                                case "U0121000a":
                                    Invoke(new Action(() => textBox1.Text += "U0121 - Нет связи с абс " + Environment.NewLine));
                                    break;
                                // Кузов
                                case "U01400008":
                                    Invoke(new Action(() => textBox1.Text += "U0140 - Нет связи с управления кузовом " + Environment.NewLine));
                                    break;
                                case "U0140000a":
                                    Invoke(new Action(() => textBox1.Text += "U0140 - Нет связи с управления кузовом " + Environment.NewLine));
                                    break;
                                // Двигатель
                                case "U01000008":
                                    Invoke(new Action(() => textBox1.Text += "U0100 - Нет связи с двигателем " + Environment.NewLine));
                                    break;
                                case "U0100000a":
                                    Invoke(new Action(() => textBox1.Text += "U0100 - Нет связи с двигателем " + Environment.NewLine));
                                    break;
                                // Парковка
                                case "U01590008":
                                    Invoke(new Action(() => textBox1.Text += "U0159 - Нет связи с модулем помоши при парковке " + Environment.NewLine));
                                    break;
                                case "U0159000a":
                                    Invoke(new Action(() => textBox1.Text += "U0159 - Нет связи с модулем помоши при парковке " + Environment.NewLine));
                                    break;
                                // ????
                                case "U04010008":
                                    Invoke(new Action(() => textBox1.Text += "U0401 - Недействительные данные, полученные от ECM/PCM" + Environment.NewLine));
                                    break;
                                case "U0401000a":
                                    Invoke(new Action(() => textBox1.Text += "U0401 - Недействительные данные, полученные от ECM/PCM" + Environment.NewLine));
                                    break;
                                // датчик 1
                                case "C200b2f8a":
                                    Invoke(new Action(() => textBox1.Text += "C200b - Хаотичный сигнал датчик момента 1" + Environment.NewLine));
                                    break;
                                // датчик 2
                                case "C200c2f8a":
                                    Invoke(new Action(() => textBox1.Text += "C200с - Хаотичный сигнал датчик момента 2" + Environment.NewLine));
                                    break;

                                //тут живут баги
                                case "P00000000":
                                    break;
                                case "P00000":
                                    break;
                                //тут живут баги

                                default:
                                    Invoke(new Action(() => textBox1.Text += oshibki[i] + " - неизвестная ошибка " + Environment.NewLine));
                                    break;

                            }
                        }
                    }


                }

            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void byte_to_write_Click(object sender, EventArgs e)
        {

        }

        private void выгрузка_данных_Click(object sender, EventArgs e)
        {
            if (openPort)
            {
                //Can15on.Length
                for (int i = 0; i < 500; i++)
                {
                    if (i >= 950) { break; }
                    PortSelect.Write(Can15on[i]);
                    выгрузка_данных.Value = i;
                }

                for (int i = 0; i < 500; i++)
                {
                    if (i >= 950) { break; }
                    PortSelect.Write(CanPusk[i]);
                    выгрузка_данных.Value = i;
                }

                button2.Text = "Данные выгружены";

            }
            else
            {
                button2.Text = "Порт закрыт";
            }
        }
    }
}