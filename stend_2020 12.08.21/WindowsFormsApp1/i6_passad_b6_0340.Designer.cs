﻿namespace WindowsFormsApp1
{
    partial class i6_passad_b6_0340 // i6_passad_b6_0340
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.can_send = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.Данные_отправка = new System.Windows.Forms.Button();
            this.Зажигание_15 = new System.Windows.Forms.Button();
            this.Питание_30 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.time_form = new System.Windows.Forms.Timer(this.components);
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.наряжение_бар = new System.Windows.Forms.ProgressBar();
            this.ток_бар = new System.Windows.Forms.ProgressBar();
            this.напряжение_текст = new System.Windows.Forms.TextBox();
            this.ток_текст = new System.Windows.Forms.TextBox();
            this.byte_to_write = new System.Windows.Forms.ProgressBar();
            this.byte_to_read = new System.Windows.Forms.ProgressBar();
            this.byte_to_read_text = new System.Windows.Forms.TextBox();
            this.byte_to_write_text = new System.Windows.Forms.TextBox();
            this.выгрузка_данных = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Кнопка_момент_мотора = new System.Windows.Forms.Button();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.textBox4 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // can_send
            // 
            this.can_send.Interval = 1000;
            this.can_send.Tick += new System.EventHandler(this.can_send_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(365, 190);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(149, 62);
            this.button4.TabIndex = 29;
            this.button4.Text = "Запрос версии БУ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(184, 190);
            this.button7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(149, 62);
            this.button7.TabIndex = 28;
            this.button7.Text = " ";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // Данные_отправка
            // 
            this.Данные_отправка.Location = new System.Drawing.Point(283, 87);
            this.Данные_отправка.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Данные_отправка.Name = "Данные_отправка";
            this.Данные_отправка.Size = new System.Drawing.Size(109, 87);
            this.Данные_отправка.TabIndex = 27;
            this.Данные_отправка.Text = "Двигатель заглушен";
            this.Данные_отправка.UseVisualStyleBackColor = true;
            this.Данные_отправка.Click += new System.EventHandler(this.Данные_отправка_Click);
            // 
            // Зажигание_15
            // 
            this.Зажигание_15.Location = new System.Drawing.Point(144, 87);
            this.Зажигание_15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Зажигание_15.Name = "Зажигание_15";
            this.Зажигание_15.Size = new System.Drawing.Size(112, 87);
            this.Зажигание_15.TabIndex = 26;
            this.Зажигание_15.Text = "Зажигание отключено";
            this.Зажигание_15.UseVisualStyleBackColor = true;
            this.Зажигание_15.Click += new System.EventHandler(this.Зажигание_15_Click);
            // 
            // Питание_30
            // 
            this.Питание_30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Питание_30.Location = new System.Drawing.Point(15, 87);
            this.Питание_30.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Питание_30.Name = "Питание_30";
            this.Питание_30.Size = new System.Drawing.Size(109, 87);
            this.Питание_30.TabIndex = 25;
            this.Питание_30.Text = "Питание отключено";
            this.Питание_30.UseVisualStyleBackColor = true;
            this.Питание_30.Click += new System.EventHandler(this.Питание_30_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(1000, 142);
            this.textBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(104, 22);
            this.textBox2.TabIndex = 24;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(613, 138);
            this.progressBar3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar3.Maximum = 1000;
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.progressBar3.RightToLeftLayout = true;
            this.progressBar3.Size = new System.Drawing.Size(182, 30);
            this.progressBar3.TabIndex = 23;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(801, 138);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar1.Maximum = 1000;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(182, 30);
            this.progressBar1.TabIndex = 20;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            this.chart1.Cursor = System.Windows.Forms.Cursors.No;
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(608, 178);
            this.chart1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart1.MaximumSize = new System.Drawing.Size(500, 500);
            this.chart1.MinimumSize = new System.Drawing.Size(300, 300);
            this.chart1.Name = "chart1";
            this.chart1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            series4.ChartArea = "ChartArea1";
            series4.LabelAngle = 90;
            series4.Legend = "Legend1";
            series4.Name = "Нм_1";
            series5.ChartArea = "ChartArea1";
            series5.LabelForeColor = System.Drawing.Color.Silver;
            series5.Legend = "Legend1";
            series5.Name = "Нм_2";
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Size = new System.Drawing.Size(500, 430);
            this.chart1.TabIndex = 21;
            this.chart1.Text = "chart1";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(15, 190);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(149, 62);
            this.button3.TabIndex = 19;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(141, 14);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(112, 57);
            this.button2.TabIndex = 18;
            this.button2.Text = "Выгрузить данные";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.MenuText;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox1.Location = new System.Drawing.Point(12, 268);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.MaxLength = 12;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(553, 392);
            this.textBox1.TabIndex = 17;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(15, 14);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 57);
            this.button1.TabIndex = 15;
            this.button1.Text = "Открыть порт";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // time_form
            // 
            this.time_form.Enabled = true;
            this.time_form.Interval = 1;
            this.time_form.Tick += new System.EventHandler(this.time_form_Tick);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(1449, 142);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(127, 22);
            this.textBox3.TabIndex = 30;
            // 
            // chart2
            // 
            chartArea4.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea4);
            this.chart2.Cursor = System.Windows.Forms.Cursors.No;
            legend4.Name = "Legend1";
            this.chart2.Legends.Add(legend4);
            this.chart2.Location = new System.Drawing.Point(1153, 178);
            this.chart2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart2.MaximumSize = new System.Drawing.Size(500, 500);
            this.chart2.MinimumSize = new System.Drawing.Size(300, 300);
            this.chart2.Name = "chart2";
            this.chart2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series6.IsVisibleInLegend = false;
            series6.Legend = "Legend1";
            series6.Name = "момент_мотора";
            this.chart2.Series.Add(series6);
            this.chart2.Size = new System.Drawing.Size(500, 430);
            this.chart2.TabIndex = 32;
            this.chart2.Text = "chart2";
            // 
            // наряжение_бар
            // 
            this.наряжение_бар.Location = new System.Drawing.Point(35, 686);
            this.наряжение_бар.Margin = new System.Windows.Forms.Padding(4);
            this.наряжение_бар.Maximum = 3500;
            this.наряжение_бар.Name = "наряжение_бар";
            this.наряжение_бар.Size = new System.Drawing.Size(433, 28);
            this.наряжение_бар.TabIndex = 33;
            // 
            // ток_бар
            // 
            this.ток_бар.Location = new System.Drawing.Point(33, 740);
            this.ток_бар.Margin = new System.Windows.Forms.Padding(4);
            this.ток_бар.Maximum = 3500;
            this.ток_бар.Name = "ток_бар";
            this.ток_бар.Size = new System.Drawing.Size(435, 28);
            this.ток_бар.TabIndex = 34;
            // 
            // напряжение_текст
            // 
            this.напряжение_текст.Location = new System.Drawing.Point(497, 689);
            this.напряжение_текст.Margin = new System.Windows.Forms.Padding(4);
            this.напряжение_текст.Name = "напряжение_текст";
            this.напряжение_текст.Size = new System.Drawing.Size(68, 22);
            this.напряжение_текст.TabIndex = 35;
            // 
            // ток_текст
            // 
            this.ток_текст.Location = new System.Drawing.Point(497, 743);
            this.ток_текст.Margin = new System.Windows.Forms.Padding(4);
            this.ток_текст.Name = "ток_текст";
            this.ток_текст.Size = new System.Drawing.Size(68, 22);
            this.ток_текст.TabIndex = 36;
            // 
            // byte_to_write
            // 
            this.byte_to_write.Location = new System.Drawing.Point(1331, 731);
            this.byte_to_write.Margin = new System.Windows.Forms.Padding(4);
            this.byte_to_write.Maximum = 5010;
            this.byte_to_write.Name = "byte_to_write";
            this.byte_to_write.Size = new System.Drawing.Size(168, 25);
            this.byte_to_write.TabIndex = 37;
            // 
            // byte_to_read
            // 
            this.byte_to_read.Location = new System.Drawing.Point(1331, 689);
            this.byte_to_read.Margin = new System.Windows.Forms.Padding(4);
            this.byte_to_read.Maximum = 5010;
            this.byte_to_read.Name = "byte_to_read";
            this.byte_to_read.Size = new System.Drawing.Size(168, 25);
            this.byte_to_read.TabIndex = 38;
            // 
            // byte_to_read_text
            // 
            this.byte_to_read_text.Location = new System.Drawing.Point(1535, 689);
            this.byte_to_read_text.Margin = new System.Windows.Forms.Padding(4);
            this.byte_to_read_text.Name = "byte_to_read_text";
            this.byte_to_read_text.Size = new System.Drawing.Size(76, 22);
            this.byte_to_read_text.TabIndex = 39;
            // 
            // byte_to_write_text
            // 
            this.byte_to_write_text.Location = new System.Drawing.Point(1535, 731);
            this.byte_to_write_text.Margin = new System.Windows.Forms.Padding(4);
            this.byte_to_write_text.Name = "byte_to_write_text";
            this.byte_to_write_text.Size = new System.Drawing.Size(76, 22);
            this.byte_to_write_text.TabIndex = 40;
            // 
            // выгрузка_данных
            // 
            this.выгрузка_данных.Location = new System.Drawing.Point(1331, 642);
            this.выгрузка_данных.Margin = new System.Windows.Forms.Padding(4);
            this.выгрузка_данных.MarqueeAnimationSpeed = 10;
            this.выгрузка_данных.Maximum = 500;
            this.выгрузка_данных.Name = "выгрузка_данных";
            this.выгрузка_данных.Size = new System.Drawing.Size(168, 25);
            this.выгрузка_данных.Step = 2;
            this.выгрузка_данных.TabIndex = 41;
            this.выгрузка_данных.Click += new System.EventHandler(this.выгрузка_данных_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(1156, 23);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(351, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // Кнопка_момент_мотора
            // 
            this.Кнопка_момент_мотора.Location = new System.Drawing.Point(1156, 138);
            this.Кнопка_момент_мотора.Name = "Кнопка_момент_мотора";
            this.Кнопка_момент_мотора.Size = new System.Drawing.Size(241, 30);
            this.Кнопка_момент_мотора.TabIndex = 44;
            this.Кнопка_момент_мотора.Text = "Мотор Выкл";
            this.Кнопка_момент_мотора.UseVisualStyleBackColor = true;
            this.Кнопка_момент_мотора.Click += new System.EventHandler(this.Кнопка_момент_мотора_Click);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(801, 102);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(182, 30);
            this.progressBar2.TabIndex = 45;
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(613, 102);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.progressBar4.RightToLeftLayout = true;
            this.progressBar4.Size = new System.Drawing.Size(182, 31);
            this.progressBar4.TabIndex = 46;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(1000, 102);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(104, 22);
            this.textBox4.TabIndex = 47;
            // 
            // i6_passad_b6_0340
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1695, 807);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.progressBar4);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.Кнопка_момент_мотора);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.выгрузка_данных);
            this.Controls.Add(this.byte_to_write_text);
            this.Controls.Add(this.byte_to_read_text);
            this.Controls.Add(this.byte_to_read);
            this.Controls.Add(this.byte_to_write);
            this.Controls.Add(this.ток_текст);
            this.Controls.Add(this.напряжение_текст);
            this.Controls.Add(this.ток_бар);
            this.Controls.Add(this.наряжение_бар);
            this.Controls.Add(this.chart2);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.Данные_отправка);
            this.Controls.Add(this.Зажигание_15);
            this.Controls.Add(this.Питание_30);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.progressBar3);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "i6_passad_b6_0340";
            this.Text = "Рулевая рейка 3GS0340";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Timer can_send;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button Данные_отправка;
        private System.Windows.Forms.Button Зажигание_15;
        private System.Windows.Forms.Button Питание_30;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.ProgressBar progressBar1;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Timer time_form;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.ProgressBar наряжение_бар;
        private System.Windows.Forms.ProgressBar ток_бар;
        private System.Windows.Forms.TextBox напряжение_текст;
        private System.Windows.Forms.TextBox ток_текст;
        private System.Windows.Forms.ProgressBar byte_to_write;
        private System.Windows.Forms.ProgressBar byte_to_read;
        private System.Windows.Forms.TextBox byte_to_read_text;
        private System.Windows.Forms.TextBox byte_to_write_text;
        private System.Windows.Forms.ProgressBar выгрузка_данных;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Кнопка_момент_мотора;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.TextBox textBox4;
    }
}