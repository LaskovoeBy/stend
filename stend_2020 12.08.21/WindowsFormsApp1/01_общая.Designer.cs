﻿namespace WindowsFormsApp1
{
    partial class общая
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.ComSelect = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.Pitanie = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.button4 = new System.Windows.Forms.Button();
            this.can_send = new System.Windows.Forms.Timer(this.components);
            this.форма_время = new System.Windows.Forms.Timer(this.components);
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.BYTE1 = new System.Windows.Forms.Button();
            this.BYTE2 = new System.Windows.Forms.Button();
            this.BYTE3 = new System.Windows.Forms.Button();
            this.BYTE4 = new System.Windows.Forms.Button();
            this.BYTE5 = new System.Windows.Forms.Button();
            this.BYTE6 = new System.Windows.Forms.Button();
            this.BYTE7 = new System.Windows.Forms.Button();
            this.BYTE8 = new System.Windows.Forms.Button();
            this.BYTE_INT = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(106, 17);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 19);
            this.button1.TabIndex = 0;
            this.button1.Text = "Открыть порт";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ComSelect
            // 
            this.ComSelect.FormattingEnabled = true;
            this.ComSelect.Location = new System.Drawing.Point(8, 16);
            this.ComSelect.Margin = new System.Windows.Forms.Padding(2);
            this.ComSelect.Name = "ComSelect";
            this.ComSelect.Size = new System.Drawing.Size(92, 21);
            this.ComSelect.TabIndex = 1;
            this.ComSelect.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.MenuText;
            this.textBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox1.Location = new System.Drawing.Point(9, 218);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.MaxLength = 12;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(252, 180);
            this.textBox1.TabIndex = 2;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(201, 17);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(91, 19);
            this.button2.TabIndex = 3;
            this.button2.Text = "Вывод данных";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(8, 153);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 50);
            this.button3.TabIndex = 4;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(466, 70);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(161, 19);
            this.progressBar1.TabIndex = 5;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Cursor = System.Windows.Forms.Cursors.No;
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(314, 154);
            this.chart1.Margin = new System.Windows.Forms.Padding(2);
            this.chart1.MaximumSize = new System.Drawing.Size(375, 406);
            this.chart1.MinimumSize = new System.Drawing.Size(225, 244);
            this.chart1.Name = "chart1";
            this.chart1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(375, 244);
            this.chart1.TabIndex = 6;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(310, 110);
            this.progressBar2.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(316, 19);
            this.progressBar2.TabIndex = 7;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(310, 70);
            this.progressBar3.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.progressBar3.RightToLeftLayout = true;
            this.progressBar3.Size = new System.Drawing.Size(142, 19);
            this.progressBar3.TabIndex = 8;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(651, 71);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(116, 20);
            this.textBox2.TabIndex = 9;
            // 
            // Pitanie
            // 
            this.Pitanie.Location = new System.Drawing.Point(9, 67);
            this.Pitanie.Margin = new System.Windows.Forms.Padding(2);
            this.Pitanie.Name = "Pitanie";
            this.Pitanie.Size = new System.Drawing.Size(70, 63);
            this.Pitanie.TabIndex = 10;
            this.Pitanie.Text = "Питание отключено";
            this.Pitanie.UseVisualStyleBackColor = true;
            this.Pitanie.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(83, 67);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(72, 63);
            this.button5.TabIndex = 11;
            this.button5.Text = "Зажигание отключено";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(160, 67);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(70, 63);
            this.button6.TabIndex = 12;
            this.button6.Text = "Двигатель заглушен";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(126, 153);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(104, 50);
            this.button7.TabIndex = 13;
            this.button7.Text = " ";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(692, 110);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 14;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // can_send
            // 
            this.can_send.Enabled = true;
            this.can_send.Interval = 10;
            this.can_send.Tick += new System.EventHandler(this.can_send_Tick);
            // 
            // форма_время
            // 
            this.форма_время.Enabled = true;
            this.форма_время.Interval = 1;
            this.форма_время.Tick += new System.EventHandler(this.форма_время_Tick);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(30, 443);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 15;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // BYTE1
            // 
            this.BYTE1.Location = new System.Drawing.Point(165, 443);
            this.BYTE1.Name = "BYTE1";
            this.BYTE1.Size = new System.Drawing.Size(37, 23);
            this.BYTE1.TabIndex = 16;
            this.BYTE1.Text = "button8";
            this.BYTE1.UseVisualStyleBackColor = true;
            this.BYTE1.Click += new System.EventHandler(this.button8_Click);
            // 
            // BYTE2
            // 
            this.BYTE2.Location = new System.Drawing.Point(208, 443);
            this.BYTE2.Name = "BYTE2";
            this.BYTE2.Size = new System.Drawing.Size(37, 23);
            this.BYTE2.TabIndex = 17;
            this.BYTE2.Text = "button9";
            this.BYTE2.UseVisualStyleBackColor = true;
            this.BYTE2.Click += new System.EventHandler(this.button9_Click);
            // 
            // BYTE3
            // 
            this.BYTE3.Location = new System.Drawing.Point(251, 443);
            this.BYTE3.Name = "BYTE3";
            this.BYTE3.Size = new System.Drawing.Size(37, 23);
            this.BYTE3.TabIndex = 18;
            this.BYTE3.Text = "button10";
            this.BYTE3.UseVisualStyleBackColor = true;
            this.BYTE3.Click += new System.EventHandler(this.button10_Click);
            // 
            // BYTE4
            // 
            this.BYTE4.Location = new System.Drawing.Point(294, 443);
            this.BYTE4.Name = "BYTE4";
            this.BYTE4.Size = new System.Drawing.Size(37, 23);
            this.BYTE4.TabIndex = 19;
            this.BYTE4.Text = "button11";
            this.BYTE4.UseVisualStyleBackColor = true;
            this.BYTE4.Click += new System.EventHandler(this.button11_Click);
            // 
            // BYTE5
            // 
            this.BYTE5.Location = new System.Drawing.Point(337, 443);
            this.BYTE5.Name = "BYTE5";
            this.BYTE5.Size = new System.Drawing.Size(37, 23);
            this.BYTE5.TabIndex = 20;
            this.BYTE5.Text = "button12";
            this.BYTE5.UseVisualStyleBackColor = true;
            this.BYTE5.Click += new System.EventHandler(this.button12_Click);
            // 
            // BYTE6
            // 
            this.BYTE6.Location = new System.Drawing.Point(380, 443);
            this.BYTE6.Name = "BYTE6";
            this.BYTE6.Size = new System.Drawing.Size(37, 23);
            this.BYTE6.TabIndex = 21;
            this.BYTE6.Text = "button13";
            this.BYTE6.UseVisualStyleBackColor = true;
            // 
            // BYTE7
            // 
            this.BYTE7.Location = new System.Drawing.Point(423, 443);
            this.BYTE7.Name = "BYTE7";
            this.BYTE7.Size = new System.Drawing.Size(37, 23);
            this.BYTE7.TabIndex = 22;
            this.BYTE7.Text = "button14";
            this.BYTE7.UseVisualStyleBackColor = true;
            // 
            // BYTE8
            // 
            this.BYTE8.Location = new System.Drawing.Point(466, 443);
            this.BYTE8.Name = "BYTE8";
            this.BYTE8.Size = new System.Drawing.Size(37, 23);
            this.BYTE8.TabIndex = 23;
            this.BYTE8.Text = "button15";
            this.BYTE8.UseVisualStyleBackColor = true;
            // 
            // BYTE_INT
            // 
            this.BYTE_INT.Location = new System.Drawing.Point(165, 473);
            this.BYTE_INT.Name = "BYTE_INT";
            this.BYTE_INT.Size = new System.Drawing.Size(110, 23);
            this.BYTE_INT.TabIndex = 24;
            this.BYTE_INT.Text = "BYTE ИЛИ ИНТ";
            this.BYTE_INT.UseVisualStyleBackColor = true;
            this.BYTE_INT.Click += new System.EventHandler(this.BYTE_INT_Click);
            // 
            // общая
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 519);
            this.Controls.Add(this.BYTE_INT);
            this.Controls.Add(this.BYTE8);
            this.Controls.Add(this.BYTE7);
            this.Controls.Add(this.BYTE6);
            this.Controls.Add(this.BYTE5);
            this.Controls.Add(this.BYTE4);
            this.Controls.Add(this.BYTE3);
            this.Controls.Add(this.BYTE2);
            this.Controls.Add(this.BYTE1);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Pitanie);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.progressBar3);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.ComSelect);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "общая";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox ComSelect;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button Pitanie;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Button button4;
        public System.Windows.Forms.Timer can_send;
        public System.Windows.Forms.Timer форма_время;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button BYTE1;
        private System.Windows.Forms.Button BYTE2;
        private System.Windows.Forms.Button BYTE3;
        private System.Windows.Forms.Button BYTE4;
        private System.Windows.Forms.Button BYTE5;
        private System.Windows.Forms.Button BYTE6;
        private System.Windows.Forms.Button BYTE7;
        private System.Windows.Forms.Button BYTE8;
        private System.Windows.Forms.Button BYTE_INT;
    }
}

