﻿namespace WindowsFormsApp1
{
    partial class i5_lend_rover_DK // i5_lend_rover_DK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Кнопка_запроса_версии_БУ = new System.Windows.Forms.Button();
            this.Кнопка_удаления_ошибок = new System.Windows.Forms.Button();
            this.Кнопка_запуск_двигателя = new System.Windows.Forms.Button();
            this.Зажигание_15 = new System.Windows.Forms.Button();
            this.Питание_30 = new System.Windows.Forms.Button();
            this.Значение_момента = new System.Windows.Forms.TextBox();
            this.Момент_влево_прогрес_бар = new System.Windows.Forms.ProgressBar();
            this.Момент_вправо_прогрес_бар = new System.Windows.Forms.ProgressBar();
            this.Таймер_10_милисекунд_элемент = new System.Windows.Forms.Timer(this.components);
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Кнопка_чтения_ошибок = new System.Windows.Forms.Button();
            this.Кнопка_выгрузить_данные = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Кнопка_открыть_порт = new System.Windows.Forms.Button();
            this.Таймер_милисекунда_элемент = new System.Windows.Forms.Timer(this.components);
            this.наряжение_бар = new System.Windows.Forms.ProgressBar();
            this.ток_бар = new System.Windows.Forms.ProgressBar();
            this.напряжение_текст = new System.Windows.Forms.TextBox();
            this.ток_текст = new System.Windows.Forms.TextBox();
            this.байтов_для_записи_бар = new System.Windows.Forms.ProgressBar();
            this.байтов_для_чтения_бар = new System.Windows.Forms.ProgressBar();
            this.байтов_для_чтения_тест_бокс = new System.Windows.Forms.TextBox();
            this.байтов_для_записи_тест_бокс = new System.Windows.Forms.TextBox();
            this.выгрузка_данных_бар = new System.Windows.Forms.ProgressBar();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Кнопка_запроса_версии_БУ
            // 
            this.Кнопка_запроса_версии_БУ.Location = new System.Drawing.Point(365, 190);
            this.Кнопка_запроса_версии_БУ.Margin = new System.Windows.Forms.Padding(4);
            this.Кнопка_запроса_версии_БУ.Name = "Кнопка_запроса_версии_БУ";
            this.Кнопка_запроса_версии_БУ.Size = new System.Drawing.Size(149, 62);
            this.Кнопка_запроса_версии_БУ.TabIndex = 29;
            this.Кнопка_запроса_версии_БУ.Text = "Текущий VIN";
            this.Кнопка_запроса_версии_БУ.UseVisualStyleBackColor = true;
            this.Кнопка_запроса_версии_БУ.Click += new System.EventHandler(this.Кнопка_запроса_версии_БУ_Click);
            // 
            // Кнопка_удаления_ошибок
            // 
            this.Кнопка_удаления_ошибок.Location = new System.Drawing.Point(184, 190);
            this.Кнопка_удаления_ошибок.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Кнопка_удаления_ошибок.Name = "Кнопка_удаления_ошибок";
            this.Кнопка_удаления_ошибок.Size = new System.Drawing.Size(149, 62);
            this.Кнопка_удаления_ошибок.TabIndex = 28;
            this.Кнопка_удаления_ошибок.Text = " ";
            this.Кнопка_удаления_ошибок.UseVisualStyleBackColor = true;
            this.Кнопка_удаления_ошибок.Click += new System.EventHandler(this.Кнопка_удаления_ошибок_Click);
            // 
            // Кнопка_запуск_двигателя
            // 
            this.Кнопка_запуск_двигателя.Location = new System.Drawing.Point(283, 87);
            this.Кнопка_запуск_двигателя.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Кнопка_запуск_двигателя.Name = "Кнопка_запуск_двигателя";
            this.Кнопка_запуск_двигателя.Size = new System.Drawing.Size(109, 87);
            this.Кнопка_запуск_двигателя.TabIndex = 27;
            this.Кнопка_запуск_двигателя.Text = "Двигатель заглушен";
            this.Кнопка_запуск_двигателя.UseVisualStyleBackColor = true;
            this.Кнопка_запуск_двигателя.Click += new System.EventHandler(this.Кнопка_запуск_двигателя_Click);
            // 
            // Зажигание_15
            // 
            this.Зажигание_15.Location = new System.Drawing.Point(144, 87);
            this.Зажигание_15.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Зажигание_15.Name = "Зажигание_15";
            this.Зажигание_15.Size = new System.Drawing.Size(112, 87);
            this.Зажигание_15.TabIndex = 26;
            this.Зажигание_15.Text = "Зажигание отключено";
            this.Зажигание_15.UseVisualStyleBackColor = true;
            this.Зажигание_15.Click += new System.EventHandler(this.Зажигание_15_Click);
            // 
            // Питание_30
            // 
            this.Питание_30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Питание_30.Location = new System.Drawing.Point(15, 87);
            this.Питание_30.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Питание_30.Name = "Питание_30";
            this.Питание_30.Size = new System.Drawing.Size(109, 87);
            this.Питание_30.TabIndex = 25;
            this.Питание_30.Text = "Питание отключено";
            this.Питание_30.UseVisualStyleBackColor = true;
            this.Питание_30.Click += new System.EventHandler(this.Питание_30_Click);
            // 
            // Значение_момента
            // 
            this.Значение_момента.Location = new System.Drawing.Point(964, 123);
            this.Значение_момента.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Значение_момента.Name = "Значение_момента";
            this.Значение_момента.Size = new System.Drawing.Size(104, 22);
            this.Значение_момента.TabIndex = 24;
            // 
            // Момент_влево_прогрес_бар
            // 
            this.Момент_влево_прогрес_бар.Location = new System.Drawing.Point(608, 124);
            this.Момент_влево_прогрес_бар.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Момент_влево_прогрес_бар.Maximum = 1000;
            this.Момент_влево_прогрес_бар.Name = "Момент_влево_прогрес_бар";
            this.Момент_влево_прогрес_бар.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Момент_влево_прогрес_бар.RightToLeftLayout = true;
            this.Момент_влево_прогрес_бар.Size = new System.Drawing.Size(159, 23);
            this.Момент_влево_прогрес_бар.TabIndex = 23;
            // 
            // Момент_вправо_прогрес_бар
            // 
            this.Момент_вправо_прогрес_бар.Location = new System.Drawing.Point(780, 124);
            this.Момент_вправо_прогрес_бар.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Момент_вправо_прогрес_бар.Maximum = 1000;
            this.Момент_вправо_прогрес_бар.Name = "Момент_вправо_прогрес_бар";
            this.Момент_вправо_прогрес_бар.Size = new System.Drawing.Size(159, 23);
            this.Момент_вправо_прогрес_бар.TabIndex = 20;
            // 
            // Таймер_10_милисекунд_элемент
            // 
            this.Таймер_10_милисекунд_элемент.Enabled = true;
            this.Таймер_10_милисекунд_элемент.Interval = 10;
            this.Таймер_10_милисекунд_элемент.Tick += new System.EventHandler(this.Таймер_10_милисекунд_элемент_Tick);
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            this.chart1.Cursor = System.Windows.Forms.Cursors.No;
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(608, 178);
            this.chart1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chart1.MaximumSize = new System.Drawing.Size(500, 500);
            this.chart1.MinimumSize = new System.Drawing.Size(300, 300);
            this.chart1.Name = "chart1";
            this.chart1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            series3.ChartArea = "ChartArea1";
            series3.LabelAngle = 90;
            series3.Legend = "Legend1";
            series3.Name = "Нм";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(500, 430);
            this.chart1.TabIndex = 21;
            this.chart1.Text = "chart1";
            // 
            // Кнопка_чтения_ошибок
            // 
            this.Кнопка_чтения_ошибок.Location = new System.Drawing.Point(15, 190);
            this.Кнопка_чтения_ошибок.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Кнопка_чтения_ошибок.Name = "Кнопка_чтения_ошибок";
            this.Кнопка_чтения_ошибок.Size = new System.Drawing.Size(149, 62);
            this.Кнопка_чтения_ошибок.TabIndex = 19;
            this.Кнопка_чтения_ошибок.UseVisualStyleBackColor = true;
            this.Кнопка_чтения_ошибок.Click += new System.EventHandler(this.Кнопка_чтения_ошибок_Click);
            // 
            // Кнопка_выгрузить_данные
            // 
            this.Кнопка_выгрузить_данные.Location = new System.Drawing.Point(141, 14);
            this.Кнопка_выгрузить_данные.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Кнопка_выгрузить_данные.Name = "Кнопка_выгрузить_данные";
            this.Кнопка_выгрузить_данные.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Кнопка_выгрузить_данные.Size = new System.Drawing.Size(112, 57);
            this.Кнопка_выгрузить_данные.TabIndex = 18;
            this.Кнопка_выгрузить_данные.Text = "Момент ограничен";
            this.Кнопка_выгрузить_данные.UseVisualStyleBackColor = true;
            this.Кнопка_выгрузить_данные.Click += new System.EventHandler(this.Кнопка_выгрузить_данные_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.MenuText;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox1.Location = new System.Drawing.Point(12, 268);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.MaxLength = 12;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(553, 392);
            this.textBox1.TabIndex = 17;
            // 
            // Кнопка_открыть_порт
            // 
            this.Кнопка_открыть_порт.Location = new System.Drawing.Point(15, 14);
            this.Кнопка_открыть_порт.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Кнопка_открыть_порт.Name = "Кнопка_открыть_порт";
            this.Кнопка_открыть_порт.Size = new System.Drawing.Size(107, 57);
            this.Кнопка_открыть_порт.TabIndex = 15;
            this.Кнопка_открыть_порт.Text = "Открыть порт";
            this.Кнопка_открыть_порт.UseVisualStyleBackColor = true;
            this.Кнопка_открыть_порт.Click += new System.EventHandler(this.Кнопка_открыть_порт_Click);
            // 
            // Таймер_милисекунда_элемент
            // 
            this.Таймер_милисекунда_элемент.Enabled = true;
            this.Таймер_милисекунда_элемент.Interval = 1;
            this.Таймер_милисекунда_элемент.Tick += new System.EventHandler(this.time_form_Tick);
            // 
            // наряжение_бар
            // 
            this.наряжение_бар.Location = new System.Drawing.Point(35, 686);
            this.наряжение_бар.Margin = new System.Windows.Forms.Padding(4);
            this.наряжение_бар.Maximum = 4005;
            this.наряжение_бар.Name = "наряжение_бар";
            this.наряжение_бар.Size = new System.Drawing.Size(433, 28);
            this.наряжение_бар.TabIndex = 33;
            // 
            // ток_бар
            // 
            this.ток_бар.Location = new System.Drawing.Point(33, 740);
            this.ток_бар.Margin = new System.Windows.Forms.Padding(4);
            this.ток_бар.Maximum = 3500;
            this.ток_бар.Name = "ток_бар";
            this.ток_бар.Size = new System.Drawing.Size(435, 28);
            this.ток_бар.TabIndex = 34;
            // 
            // напряжение_текст
            // 
            this.напряжение_текст.Location = new System.Drawing.Point(497, 689);
            this.напряжение_текст.Margin = new System.Windows.Forms.Padding(4);
            this.напряжение_текст.Name = "напряжение_текст";
            this.напряжение_текст.Size = new System.Drawing.Size(68, 22);
            this.напряжение_текст.TabIndex = 35;
            // 
            // ток_текст
            // 
            this.ток_текст.Location = new System.Drawing.Point(497, 743);
            this.ток_текст.Margin = new System.Windows.Forms.Padding(4);
            this.ток_текст.Name = "ток_текст";
            this.ток_текст.Size = new System.Drawing.Size(68, 22);
            this.ток_текст.TabIndex = 36;
            // 
            // байтов_для_записи_бар
            // 
            this.байтов_для_записи_бар.Location = new System.Drawing.Point(1331, 731);
            this.байтов_для_записи_бар.Margin = new System.Windows.Forms.Padding(4);
            this.байтов_для_записи_бар.Maximum = 5010;
            this.байтов_для_записи_бар.Name = "байтов_для_записи_бар";
            this.байтов_для_записи_бар.Size = new System.Drawing.Size(168, 25);
            this.байтов_для_записи_бар.TabIndex = 37;
            // 
            // байтов_для_чтения_бар
            // 
            this.байтов_для_чтения_бар.Location = new System.Drawing.Point(1331, 689);
            this.байтов_для_чтения_бар.Margin = new System.Windows.Forms.Padding(4);
            this.байтов_для_чтения_бар.Maximum = 5010;
            this.байтов_для_чтения_бар.Name = "байтов_для_чтения_бар";
            this.байтов_для_чтения_бар.Size = new System.Drawing.Size(168, 25);
            this.байтов_для_чтения_бар.TabIndex = 38;
            // 
            // байтов_для_чтения_тест_бокс
            // 
            this.байтов_для_чтения_тест_бокс.Location = new System.Drawing.Point(1535, 689);
            this.байтов_для_чтения_тест_бокс.Margin = new System.Windows.Forms.Padding(4);
            this.байтов_для_чтения_тест_бокс.Name = "байтов_для_чтения_тест_бокс";
            this.байтов_для_чтения_тест_бокс.Size = new System.Drawing.Size(76, 22);
            this.байтов_для_чтения_тест_бокс.TabIndex = 39;
            // 
            // байтов_для_записи_тест_бокс
            // 
            this.байтов_для_записи_тест_бокс.Location = new System.Drawing.Point(1535, 731);
            this.байтов_для_записи_тест_бокс.Margin = new System.Windows.Forms.Padding(4);
            this.байтов_для_записи_тест_бокс.Name = "байтов_для_записи_тест_бокс";
            this.байтов_для_записи_тест_бокс.Size = new System.Drawing.Size(76, 22);
            this.байтов_для_записи_тест_бокс.TabIndex = 40;
            // 
            // выгрузка_данных_бар
            // 
            this.выгрузка_данных_бар.Location = new System.Drawing.Point(1331, 642);
            this.выгрузка_данных_бар.Margin = new System.Windows.Forms.Padding(4);
            this.выгрузка_данных_бар.MarqueeAnimationSpeed = 10;
            this.выгрузка_данных_бар.Maximum = 500;
            this.выгрузка_данных_бар.Name = "выгрузка_данных_бар";
            this.выгрузка_данных_бар.Size = new System.Drawing.Size(168, 25);
            this.выгрузка_данных_бар.Step = 2;
            this.выгрузка_данных_бар.TabIndex = 41;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(757, 14);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(351, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // i5_lend_rover_DK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 807);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.выгрузка_данных_бар);
            this.Controls.Add(this.байтов_для_записи_тест_бокс);
            this.Controls.Add(this.байтов_для_чтения_тест_бокс);
            this.Controls.Add(this.байтов_для_чтения_бар);
            this.Controls.Add(this.байтов_для_записи_бар);
            this.Controls.Add(this.ток_текст);
            this.Controls.Add(this.напряжение_текст);
            this.Controls.Add(this.ток_бар);
            this.Controls.Add(this.наряжение_бар);
            this.Controls.Add(this.Кнопка_запроса_версии_БУ);
            this.Controls.Add(this.Кнопка_удаления_ошибок);
            this.Controls.Add(this.Кнопка_запуск_двигателя);
            this.Controls.Add(this.Зажигание_15);
            this.Controls.Add(this.Питание_30);
            this.Controls.Add(this.Значение_момента);
            this.Controls.Add(this.Момент_влево_прогрес_бар);
            this.Controls.Add(this.Момент_вправо_прогрес_бар);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.Кнопка_чтения_ошибок);
            this.Controls.Add(this.Кнопка_выгрузить_данные);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Кнопка_открыть_порт);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "i5_lend_rover_DK";
            this.Text = "03,";
            this.Load += new System.EventHandler(this.Form3_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Кнопка_запроса_версии_БУ;
        private System.Windows.Forms.Button Кнопка_удаления_ошибок;
        private System.Windows.Forms.Button Кнопка_запуск_двигателя;
        private System.Windows.Forms.Button Зажигание_15;
        private System.Windows.Forms.Button Питание_30;
        private System.Windows.Forms.TextBox Значение_момента;
        private System.Windows.Forms.ProgressBar Момент_влево_прогрес_бар;
        private System.Windows.Forms.ProgressBar Момент_вправо_прогрес_бар;
        public System.Windows.Forms.Timer Таймер_10_милисекунд_элемент;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button Кнопка_чтения_ошибок;
        private System.Windows.Forms.Button Кнопка_выгрузить_данные;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Кнопка_открыть_порт;
        public System.Windows.Forms.Timer Таймер_милисекунда_элемент;
        private System.Windows.Forms.ProgressBar наряжение_бар;
        private System.Windows.Forms.ProgressBar ток_бар;
        private System.Windows.Forms.TextBox напряжение_текст;
        private System.Windows.Forms.TextBox ток_текст;
        private System.Windows.Forms.ProgressBar байтов_для_записи_бар;
        private System.Windows.Forms.ProgressBar байтов_для_чтения_бар;
        private System.Windows.Forms.TextBox байтов_для_чтения_тест_бокс;
        private System.Windows.Forms.TextBox байтов_для_записи_тест_бокс;
        private System.Windows.Forms.ProgressBar выгрузка_данных_бар;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}