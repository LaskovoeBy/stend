﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        string ComPortSelect;
        static Boolean openPort = false;
        static SerialPort PortSelect;
        string Выбрана_рейка; // номера реек из выпадающего списка лучше хранить в базе
        string[] nomer_reiki = {  "2601", "2602","2603","2604","2605","2606" ,"2614","2617","2619","2620","2626","0340", "0344", "0345", "0346",  "0350" , "4408", "5632" , "5635", "4408"//, "", "", "", "", "" 
        };
        public Form2()
        {

            InitializeComponent();
            string[] ports = SerialPort.GetPortNames();
            comboBox1.Items.AddRange(ports);
            button1.Text = ("Выберите порт");
            comboBox2.Items.AddRange(nomer_reiki);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openPort)
            {
                openPort = false;
                button1.Text = ("Пройти верификацию");
                string[] ports = SerialPort.GetPortNames();
                comboBox1.Items.Clear();
                comboBox1.Items.AddRange(ports);


            }
            else
            {
                openPort = true;
                // почему в функции чтения порта, так сложно все и тянутся файлы, лучше использоваться трай кочь 
                // человек может быть тупым и не всегда у него может завестись с первого раза
                try
                {
                    PortSelect = new SerialPort(ComPortSelect, 250000);// почему 250 ?
                    PortSelect.Open();
                    PortSelect.WriteLine("$7777;"); // что за 7777 ?
                    int i = 0;
                    while (PortSelect.BytesToRead != 0)
                    {
                        i++; //какой-то счетчик, если он не проходит, то ошибка. 
                        string верификация = PortSelect.ReadLine();
                        if (верификация == "159885\r")
                        {
                            string путь_к_файлу = @"C: \Users\Public\Documents\file_gs.txt"; // пути к файлу лучше пришить к проекту
                            StreamWriter файл_в_корне = new StreamWriter(путь_к_файлу, false);
                            файл_в_корне.WriteLine(ComPortSelect);
                            button1.Text = "Верификация пройдена";
                            button1.BackColor = Color.Transparent;
                            файл_в_корне.Close();
                            break;

                        }
                        else if (i == 1000000)
                        {
                            button1.Text = "Ошибка верификации";
                            break;
                        }
                    }

                    PortSelect.DiscardInBuffer();
                    PortSelect.DiscardOutBuffer();
                    PortSelect.Close();


                }
                catch (IOException)
                {
                    MessageBox.Show("Выбран неверный порт");
                    button1.Text = "Перезагрузить список портов";
                }//почти хорошо, но нужно закрыть порт и обнулить все, заставив пользователя все сделать заного

               
                   
                   
               
                

            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComPortSelect = comboBox1.SelectedItem.ToString();
            button1.Text = ("Пройти верификацию");
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0000.jpg");  //подгрузка формы писал выше, лучше к проекту присоеденить
            pictureBox2.Show();

            string путь_к_файлу = @"C: \Users\Public\Documents\file_gs.txt";

            button1.Text = "Верификация пройдена";
            button1.BackColor = Color.Transparent;// а где обнуления до исходного состояния, или к этому нельзя вернуться принципиально ?

            if (File.Exists(путь_к_файлу))
            {
                StreamReader файл_в_корне = new StreamReader(путь_к_файлу, Encoding.Default);
                if (файл_в_корне.ReadLine() == null)
                {
                   
                }
                файл_в_корне.Close();
            }
            
            else
            {
                button1.Text = "Верификация не пройдена, выберите порт";
                button1.BackColor = Color.FromArgb(255, 0, 0);//проще использовать стандартные цвета т.е. это будет выглядеть вот так
                                                              //button1.BackColor = Color.Red;
                                                              //можно потерять политру цветов, если у тебя кнопки будут всегда разные
               
            }
           
           
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedItem.ToString() != null)//а проверка на дурака, если человек сам что-то захочет написать, если список реек будет внушительным
            {
                Выбрана_рейка = comboBox2.SelectedItem.ToString();
                button2.Text = Выбрана_рейка;
            }
            else button2.Text = "Рулевая рейка не найдена";

            switch (Выбрана_рейка)
            {
                //когда вибираешь рейки, текст в текстбоксе без форматирования, сложно читать, можно и тут исправить, но лучше откуда-то подгружать
                // как и сами наименования реек. 
                case "4408":
                    button2.Text =  "Mercedes GLK X204";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\4408.jpg");
                    pictureBox1.Show();
                    break;


                case "2604": //+
                    button2.Text = "Ford Explorer 2011-"; // Environment.NewLine почему не использовал /n ? ¯\_(ツ)_/¯
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2604.jpg");
                    textBox1.Text = "Крепление к кузову резьбовые" + Environment.NewLine + "Без системы автоматической парковки" + 
                    "Оригинальные номера детали: EB5Z3504TRM, EB5Z3504P, EB5Z3504L, EB5Z3504G, EB5Z3504LE, EB5Z3504H, EB5Z3504D, EB5Z3504A, DB5Z3504KE, DB5Z3504AE";
                    pictureBox1.Show();
                    break;

                case "2602": //+
                    button2.Text = "Ford Explorer 2011-";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2602.jpg");
                    textBox1.Text ="Крепление к кузову автомобиля через салентблоки"+ Environment.NewLine + "Без системы автоматической парковки" + Environment.NewLine +
                    "Оригинальные номера детали: DB5Z3504H, DB5Z3504NE, DB5Z3504MRM, DB5Z3504L, DB5Z3504F, DB5Z3504E, DB5Z3504CE, DB5Z3504C, DB5Z3504HE, DB5Z3504FE, BB5Z3504ARM, BB533200EB, BB5Z3504AARM, " +
                    "BB5Z3504KE, BB5Z3504ME, BB5Z3504PE, BB5Z3504J, BB5Z3504HE, BB5Z3504FE, BB5Z3504EE, BB5Z3504C, BB5Z3504BE, BB5Z3504AE";
                    pictureBox1.Show();
                    break;

                case "2603": //+
                    button2.Text = "Ford Explorer 2011-";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2602.jpg");
                    textBox1.Text ="Крепление к кузову автомобиля через сайлентблоки" + Environment.NewLine + "С системой автоматической парковки" + Environment.NewLine +
                    "Оригинальные номера детали: EB5Z3504TRM, EB5Z3504P, EB5Z3504L, EB5Z3504G, EB5Z3504LE, EB5Z3504H, EB5Z3504D, EB5Z3504A, DB5Z3504KE, DB5Z3504AE";
                    pictureBox1.Show();
                    break;

                case "2617": // +
                    button2.Text = "Ford Explorer 2011-";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2604.jpg");
                    textBox1.Text = "Крепление к кузову резьбовые" + Environment.NewLine + "Без системы автоматической парковки" + Environment.NewLine +
                     "Оригинальные номера детали: FB5Z3504AARM, FB5Z3504S, FB5Z3504V, FB5Z3504A, FB5Z3504J, FB5Z3504JE, FB5Z3504E";
                    pictureBox1.Show();
                    break;

                case "2619": // +
                    button2.Text = "Ford Explorer 2011-";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2604.jpg");
                    textBox1.Text = "Крепление к кузову автомобиля резьбовые" + Environment.NewLine + "C системой автоматической парковки" + Environment.NewLine +
                    "Оригинальные номера детали: FB5Z3504BARM, FB5Z3504W, FB5Z3504T, FB5Z3504B, FB5Z3504K, FB5Z3504KE, FB5Z3504F";
                    pictureBox1.Show();
                    break;

                case "2620": // +
                    button2.Text = "Ford Explorer 2011- ";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2604.jpg");
                    textBox1.Text = "Version Sport" + Environment.NewLine + "Крепление к кузову автомобиля резьбовые" + Environment.NewLine + "C системой автоматической парковки" + Environment.NewLine +
                     "Оригинальные номера детали: FB5Z3504CARM, FB5Z3504LE, FB5Z3504X, FB5Z3504L, FB5Z3504C, FB5Z3504R, FB5Z3504G";
                    pictureBox1.Show();
                    break;

                case "2614": // +
                    button2.Text = "Ford Explorer 2011-";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2604.jpg");
                     textBox1.Text = "Version Sport" + Environment.NewLine + "Крепление к кузову автомобиля резьбовые" + Environment.NewLine + "Без системы автоматической парковки" + Environment.NewLine +
                     "Оригинальные номера детали: EB5Z3504R, EB5Z3504N, EB5Z3504NE, EB5Z3504CARM, EB5Z3504K, EB5Z3504F, EB5Z3504C, DB5Z3504ME, DB5Z3504EE";
                    pictureBox1.Show();
                    break;

                case "2605": // +
                    button2.Text = "Ford Explorer 2011-";
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2604.jpg");
                    textBox1.Text = "Крепление к кузову автомобиля резьбовые" + Environment.NewLine + "C системой автоматической парковки" + Environment.NewLine +
                    "Оригинальные номера детали: EB5Z3504ME, EB5Z3504BARM, EB5Z3504Q, EB5Z3504M, EB5Z3504J, EB5Z3504E, EB5Z3504B, DB5Z3504LE, DB5Z3504BE";
                    pictureBox1.Show();
                    break;

                case "5635":
                    button2.Text = "Land Rover" + Environment.NewLine + "Range Rover IV" ;
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\5635.jpg");
                    textBox1.Text = "Оригинальные номера детали: FK523200AB, LR092481, LR058396, LR056316, LR046039, FK523200AA, DK523504AB, DK523504AA, DK523200CB, DK523200CA, DK523200AD, " +
                    "DK523200AC, DK523200AB, DK523200AA";
                    pictureBox1.Show();
                    break;
                   
                case "2601":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2601.jpg");
                    pictureBox1.Show();
                    button2.Text = "Ford Focus";
                    textBox1.Text = "Ford Focus " + Environment.NewLine + "Количество оборотов 2.7" + Environment.NewLine + "Устанавливается на Ford Transit/Tourneo Connect 2013-;" +
                        " Ford Kuga 2008-; Ford C-Max / Grand C-Max 2010-; Ford Focus 2011- " + Environment.NewLine + "Прошивки блоков управления:" + Environment.NewLine + "BV - Focus, Transit"
                        + Environment.NewLine + "CV - Kuga" + Environment.NewLine + "AV - С-Max, Grand C-Max";
                    break;

                case "2606":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2601.jpg");
                    pictureBox1.Show();
                    button2.Text = "Ford Focus";
                    textBox1.Text = "Ford Focus " + Environment.NewLine + "Количество оборотов 2.7" + Environment.NewLine + "Устанавливается на Ford Transit/Tourneo Connect 2013-;" +
                        " Ford Kuga 2008-; Ford C-Max / Grand C-Max 2010-; Ford Focus 2011- " + Environment.NewLine + "Прошивки блоков управления:" + Environment.NewLine +  "BV - Focus, Transit" 
                        + Environment.NewLine + "CV - Kuga" + Environment.NewLine + "AV - С-Max, Grand C-Max";
                    break;

                case "2626":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\2601.jpg");
                    pictureBox1.Show();
                    button2.Text = "Ford Focus ST" ;
                    textBox1.Text = "Ford Focus ST" + Environment.NewLine + "Количество оборотов 2.2" + Environment.NewLine + "Устанавливается на  " +
                        "Ford Focus ST 2011- " + Environment.NewLine + "Оригинальные номера: 1773654, 1779814, 1782989, 1791077, 1819764, 1831764, 1849810," +
                        " 1862701, 1869580, 1880729, 1939526, BV613D070AB, BV613D070AC, BV613D070AD, BV613D070AE, BV613D070AF, BV613D070AH, BV613D070AK, BV613D070AL," +
                        " BV613D070AM, BV613D070AN, BV613D070AP, BV613D070AR, BV613D070AS, 1897286, 1805672"; 
                    break;

                case "0000":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0000.jpg");
                    pictureBox1.Show();
                    button2.Text = "Форма общего" + "\n" + " назначения ";
                    break;
                    
                case "5004":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\5004.jpg");
                    pictureBox1.Show();
                    button2.Text = "Ситроен с3"  + "\n" + " пикасо";
                    textBox1.Text = "";
                    break;
                    
                case "5632":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\5632.jpg");
                    pictureBox1.Show();
                    button2.Text = "Land Rover" + "\n" + "Range Rover Evoqoe ";
                    textBox1.Text = "Оригинальные номера: LR088698, LR049354, EJ323504AC, EJ323504AB, EJ323504AA, EJ323200CB, EJ323200CA, EJ323200AC, EJ323200AB, EJ323200AA";
                    break;
                
                case "0340":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0340.jpg");
                    pictureBox1.Show();
                    button2.Text = "Volkswagen Passat B6" + "\n" + "Generatoin II";
                    textBox1.Text = "";
                    break;

                case "0344":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0344.jpg");
                    pictureBox1.Show();
                    button2.Text = "Volkswagen Passat B6" + "\n" + "Generatoin II";
                    textBox1.Text = "";
                    break;

                case "0345":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0345.jpg");
                    pictureBox1.Show();
                    button2.Text = "Volkswagen Tiguan" + "\n" + " 5N1909144 ";
                    textBox1.Text = "Ориганальные номера: 5N1423058EX, 5N1423058E, 5N1423062JX, 5N1423062J, 5N1423062HX, 5N1423062H, " +
                        "5N1423062FX, 5N1423062F, 5N1423062EX, 5N1423062, 5N1423061TX, 5N1423061T, 5N1423061SX, 5N1423061S, 5N1423061PX, " +
                        "5N1423061P, 5N1423061NX, 5N1423061N, 5N1423061MX, 5N1423061M, 5N1423061LX, 5N1423061L, 5N1423061KX, 5N1423061K, 5N1423061J, " +
                        "5N1909144R, 5N1909144M, 5N1909144G, 5N1423062X, 5N1423062TX, 5N1423062T, " +
                        "5N1423062RX, 5N1423062R, 5N1423062PX, 5N1423062P, 5N1423062MX, 5N1423062M, 5N1423062E, 5N1423062BX, 5N1423062B, 5N1423058CX, " +
                        "5N1423058C, 5N1423058AX, 5N1423058A, 5N1423050Q, 3AB423061G";
                    break;

                case "0350":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0350.jpg");
                    pictureBox1.Show();
                    button2.Text = "Audi A4" + "\n" + " 8K0909144 ";
                    textBox1.Text = "Ориганальные номера детали: 8K1423055AC, 8K1423055AJ, 8K1423055AN, 8K1423055AS, 8K1423055BF, 8K1423055BK," +
                        "8K1423055BR, 8K1423055CA, 8K1423055N, 8K1423055S";
                    break;
                
                case "0346":
                    pictureBox1.Image = Image.FromFile("C:\\Users\\Public\\Documents\\0346.jpg");
                    pictureBox1.Show();
                    button2.Text = "Audi Q5" + "\n" + " 8R0909144 ";
                    textBox1.Text = "Ориганальные номера детали: 8R1423055AC, 8R1423055AF, 8R1423055AJ, 8R1423055AN, 8R1423055AS, 8R1423055BE, 8R1423055BG, " +
                        "8R1423055E, 8R1423055J, 8R1423055L, 8R1423055R, 8R1423055T";
                    break;

                default:
                    button2.Text = "Рулевая рейка не найдена";
                    break;
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
             switch (Выбрана_рейка)// простота как она и есть, можно и так, но почему нельзя было эти функции вызывать выше 
            {
                case "0000":
                    общая newfrm = new общая();
                    MessageBox.Show("Для диагностики используйте провод №0");
                    newfrm.Show();
                    break;

                case "4408":
                    i7_Mersedes_166 newfrm4408 = new i7_Mersedes_166();
                    MessageBox.Show("Для диагностики используйте провод №1");
                    newfrm4408.Show();
                    break;

                case "2602":
                    i1_Ford_explorer newfrm2602 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2602.Show();
                    break;
                case "2603":
                    i1_Ford_explorer newfrm2603 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2603.Show();
                    break;
                case "2604":
                    i1_Ford_explorer newfrm2604 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2604.Show();
                    break;
                case "2605":
                    i1_Ford_explorer newfrm2605 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2605.Show();
                    break;
                case "2614":
                    i1_Ford_explorer newfrm2614 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2614.Show();
                    break;
                case "2617":
                    i1_Ford_explorer newfrm2617 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2617.Show();
                    break;
                case "2619":
                    i1_Ford_explorer newfrm2619 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2619.Show();
                    break;
                case "2620":
                    i1_Ford_explorer newfrm2620 = new i1_Ford_explorer();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2620.Show();
                    break;

                case "2601":
                    i8_focus_2601 newfrm2601 = new i8_focus_2601();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2601.Show();
                    break;
                case "2606":
                    i8_focus_2601 newfrm2606 = new i8_focus_2601();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2606.Show();
                    break;
                case "2626":
                    i8_focus_2601 newfrm2626 = new i8_focus_2601();
                    MessageBox.Show("Для диагностики используйте провод №2");
                    newfrm2626.Show();
                    break;


                case "5004":
                     _4_ситроент_с3_пикасо newfrm4 = new _4_ситроент_с3_пикасо();
                    MessageBox.Show("Для диагностики используйте провод №3");
                    newfrm4.Show();
                    break;

                
                case "0340":
                    i6_passad_b6_0340 newfrm6 = new i6_passad_b6_0340();
                    MessageBox.Show("Для диагностики используйте провод №4");
                    newfrm6.Show();
                    break;

                case "0345":
                    _6_тигуан_0345 newfrm7 = new _6_тигуан_0345();
                    MessageBox.Show("Для диагностики используйте провод №5");
                    newfrm7.Show();
                    break;

                case "0344":
                    _7_октавия_0344 newfrm8 = new _7_октавия_0344();
                    MessageBox.Show("Для диагностики используйте провод №6");
                    newfrm8.Show();
                    break;

                case "0350":
                    i2_audi_8k_0350 newfrm9 = new i2_audi_8k_0350(); //i2_audi_8k_0350
                    MessageBox.Show("Для диагностики используйте провод №7");
                    newfrm9.Show();
                    break;
                case "0346":
                    i4_audi_Q5_0346 newfrm10 = new i4_audi_Q5_0346();
                    MessageBox.Show("Для диагностики используйте провод №7");
                    newfrm10.Show();
                    break;

                case "5632":
                    i5_lend_rover_DK newfrm12 = new i5_lend_rover_DK();
                    MessageBox.Show("Для диагностики используйте провод №1");
                    newfrm12.Show();
                    break;

                case "5635":
                    i5_lend_rover_DK newfrm55 = new i5_lend_rover_DK();
                    MessageBox.Show("Для диагностики используйте провод №1");
                    newfrm55.Show();
                    break;


                default:
                    button2.Text = "Рулевая рейка не найдена";
                    break;
            }
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
           

        }
    }
}
